export interface AuthenticationResponse{
    token: string;
    id: number;
    role: string
}